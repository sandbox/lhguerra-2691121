This module simply gets all node types and creates a block with a list of links
for adding them, like content/add page.

These particular links can not be created as a block with the Menu Block module
because they are not really in any menu structure inside Drupal.

Just download and enable this module and the block will be available in the
admin's interface.
